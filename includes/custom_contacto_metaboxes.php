<?php
/* --------------------------------------------------------------
1.- CONTACTO: INFO
-------------------------------------------------------------- */
$cmb_contacto_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'contacto_info',
    'title'         => esc_html__( 'Contacto: Información Adicional', 'zunergy' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'slug', 'value' => array('contacto', 'contact-us') ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => true
) );

$cmb_contacto_metabox->add_field( array(
    'id'      => $prefix . 'contacto_info_dir',
    'name'      => esc_html__( 'Dirección', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí la dirección', 'zunergy' ),
    'type'    => 'textarea_small'
) );

$cmb_contacto_metabox->add_field( array(
    'id'      => $prefix . 'contacto_info_telf',
    'name'      => esc_html__( 'Teléfono', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí el teléfono', 'zunergy' ),
    'type'    => 'textarea_small'
) );

$cmb_contacto_metabox->add_field( array(
    'id'      => $prefix . 'contacto_info_email',
    'name'      => esc_html__( 'Correo Electrónico', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí el Correo Electrónico', 'zunergy' ),
    'type'    => 'textarea_small'
) );
