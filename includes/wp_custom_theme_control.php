<?php
/* --------------------------------------------------------------
CUSTOM AREA FOR OPTIONS DATA
-------------------------------------------------------------- */

add_action( 'customize_register', 'zunergy_customize_register' );

function zunergy_customize_register( $wp_customize ) {
    $wp_customize->add_section('zunergy_footer_settings', array(
        'title'    => __('Footer', 'zunergy'),
        'description' => __('Opciones del pie de página', 'zunergy'),
        'priority' => 31,
    ));

    $wp_customize->add_setting('zunergy_footer_settings[footer_bg]', array(
        'default'           => 'image.jpg',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'footer_bg', array(
        'label'    => __('Fondo del Pie de Pagina', 'themename'),
        'section'  => 'zunergy_footer_settings',
        'settings' => 'zunergy_footer_settings[footer_bg]',
    )));

    $wp_customize->add_section('zunergy_social_settings', array(
        'title'    => __('Redes Sociales', 'zunergy'),
        'description' => __('Redes Sociales', 'zunergy'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('zunergy_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'zunergy_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'facebook', array(
        'type' => 'url',
        'section' => 'zunergy_social_settings', // Add a default or your own section
        'settings' => 'zunergy_social_settings[facebook]',
        'label' => __( 'Facebook', 'zunergy' ),
    ) );

    $wp_customize->add_setting('zunergy_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'zunergy_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'twitter', array(
        'type' => 'url',
        'section' => 'zunergy_social_settings', // Add a default or your own section
        'settings' => 'zunergy_social_settings[twitter]',
        'label' => __( 'Twitter', 'zunergy' ),
    ) );

    $wp_customize->add_setting('zunergy_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'zunergy_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'instagram', array(
        'type' => 'url',
        'section' => 'zunergy_social_settings', // Add a default or your own section
        'settings' => 'zunergy_social_settings[instagram]',
        'label' => __( 'Instagram', 'zunergy' ),
    ) );

    $wp_customize->add_setting('zunergy_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'zunergy_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'linkedin', array(
        'type' => 'url',
        'section' => 'zunergy_social_settings', // Add a default or your own section
        'settings' => 'zunergy_social_settings[linkedin]',
        'label' => __( 'LinkedIn', 'zunergy' ),
    ) );

    $wp_customize->add_section('zunergy_cookie_settings', array(
        'title'    => __('Cookies', 'zunergy'),
        'description' => __('Opciones de Cookies', 'zunergy'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('zunergy_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'zunergy'),
        'description' => __( 'Texto del Cookie consent.' ),
        'section'  => 'zunergy_cookie_settings',
        'settings' => 'zunergy_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('zunergy_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'zunergy_cookie_settings', // Add a default or your own section
        'settings' => 'zunergy_cookie_settings[cookie_link]',
        'label' => __( 'Link de Cookies', 'zunergy' ),
    ) );
}

function zunergy_sanitize_url( $url ) {
    return esc_url_raw( $url );
}

