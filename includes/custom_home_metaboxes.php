<?php
/* --------------------------------------------------------------
1.- MAIN SLIDER
-------------------------------------------------------------- */
$cmb_home_hero = new_cmb2_box( array(
    'id'            => $prefix . 'home_hero',
    'title'         => esc_html__( 'Inicio: Hero Principal', 'zunergy' ),
    'object_types'  => array( 'page' ),
    'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'hero_background',
    'name' => esc_html__('Fondo del Hero', 'zunergy'),
    'desc' => esc_html__('Seleccione un fondo para el hero.', 'zunergy'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Cargar Fondo', // default: "Add or Upload Files"
        'remove_image_text' => 'Remover Fondo', // default: "Remove Image"
        'file_text' => 'Fondo:', // default: "File:"
        'file_download_text' => 'Descargar', // default: "Download"
        'remove_text' => 'Remover', // default: "Remove"
    )
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'hero_image',
    'name' => esc_html__('Imagen del Hero', 'zunergy'),
    'desc' => esc_html__('Seleccione una imagen descriptiva del hero.', 'zunergy'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Cargar Imagen', // default: "Add or Upload Files"
        'remove_image_text' => 'Remover Imagen', // default: "Remove Image"
        'file_text' => 'Imagen:', // default: "File:"
        'file_download_text' => 'Descargar', // default: "Download"
        'remove_text' => 'Remover', // default: "Remove"
    )
) );

$cmb_home_hero->add_field( array(
    'id'      => $prefix . 'hero_title',
    'name'      => esc_html__( 'Título del Hero', 'zunergy' ),
    'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'zunergy' ),
    'type'    => 'wysiwyg',
    'options' => array(),
) );

$cmb_home_hero->add_field( array(
    'id'      => $prefix . 'hero_description',
    'name'      => esc_html__( 'Texto del Hero', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese un texto descriptivo del Hero', 'zunergy' ),
    'type' => 'textarea_small'
) );


/* --------------------------------------------------------------
2.- FEATURES SECTION
-------------------------------------------------------------- */
$cmb_home_features = new_cmb2_box( array(
    'id'            => $prefix . 'home_features',
    'title'         => esc_html__( 'Inicio: Beneficios', 'zunergy' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => true
) );

$cmb_home_features->add_field( array(
    'id'      => $prefix . 'features_title',
    'name'      => esc_html__( 'Título de la Sección', 'zunergy' ),
    'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'zunergy' ),
    'type'    => 'wysiwyg',
    'options' => array(),
) );

$group_field_id = $cmb_home_features->add_field( array(
    'id'          => $prefix . 'home_features_group',
    'type'        => 'group',
    'description' => __( 'Beneficios', 'zunergy' ),
    'options'     => array(
        'group_title'       => __( 'Beneficio {#}', 'zunergy' ),
        'add_button'        => __( 'Agregar otro Beneficio', 'zunergy' ),
        'remove_button'     => __( 'Remover Beneficio', 'zunergy' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro que quiere eliminar este beneficio?', 'zunergy' )
    )
) );


$cmb_home_features->add_group_field( $group_field_id, array(
    'id'   => 'icon',
    'name' => esc_html__('Ícono del Beneficio', 'zunergy'),
    'desc' => esc_html__('Seleccione un icono descriptivo del beneficio.', 'zunergy'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Cargar Ícono', // default: "Add or Upload Files"
        'remove_image_text' => 'Remover Ícono', // default: "Remove Image"
        'file_text' => 'Ícono:', // default: "File:"
        'file_download_text' => 'Descargar', // default: "Download"
        'remove_text' => 'Remover', // default: "Remove"
    )
) );

$cmb_home_features->add_group_field( $group_field_id, array(
    'id'   => 'description',
    'name' => esc_html__('Descripción del Beneficio', 'zunergy'),
    'desc' => esc_html__("Ingrese un texto descriptivo del beneficio", 'zunergy'),
    'type' => 'textarea_small'
) );

/* --------------------------------------------------------------
3.- GIF SECTION
-------------------------------------------------------------- */
$cmb_home_exp = new_cmb2_box( array(
    'id'            => $prefix . 'home_explanation',
    'title'         => esc_html__( 'Inicio: Imagen gif', 'zunergy' ),
    'object_types'  => array( 'page' ),
    'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_exp->add_field( array(
    'id'   => $prefix . 'home_exp_image',
    'name' => esc_html__('GIF de la sección', 'zunergy'),
    'desc' => esc_html__('Seleccione el gif explicatorio para la sección.', 'zunergy'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Cargar GIF', // default: "Add or Upload Files"
        'remove_image_text' => 'Remover GIF', // default: "Remove Image"
        'file_text' => 'GIF:', // default: "File:"
        'file_download_text' => 'Descargar', // default: "Download"
        'remove_text' => 'Remover', // default: "Remove"
    )
) );

$cmb_home_exp->add_field( array(
    'id'      => $prefix . 'home_exp_title',
    'name'      => esc_html__( 'Título de Sección', 'zunergy' ),
    'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'zunergy' ),
    'type'    => 'wysiwyg',
    'options' => array(),
) );

$cmb_home_exp->add_field( array(
    'id'      => $prefix . 'home_exp_desc',
    'name'      => esc_html__( 'Descripción de la sección', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese un texto descriptivo de la sección', 'zunergy' ),
    'type' => 'textarea_small'
) );

/* --------------------------------------------------------------
4.- PRODUCT DESCRIPTION
-------------------------------------------------------------- */
$cmb_home_product = new_cmb2_box( array(
    'id'            => $prefix . 'home_products',
    'title'         => esc_html__( 'Inicio: Producto', 'zunergy' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => true
) );

$cmb_home_product->add_field( array(
    'id'      => $prefix . 'home_products_title',
    'name'      => esc_html__( 'Título de la Sección', 'zunergy' ),
    'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'zunergy' ),
    'type'    => 'wysiwyg',
    'options' => array(),
) );

$cmb_home_product->add_field( array(
    'id'   => $prefix . 'home_product_image',
    'name' => esc_html__('Imagen del Hero', 'zunergy'),
    'desc' => esc_html__('Seleccione una imagen descriptiva del hero.', 'zunergy'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Cargar Imagen', // default: "Add or Upload Files"
        'remove_image_text' => 'Remover Imagen', // default: "Remove Image"
        'file_text' => 'Imagen:', // default: "File:"
        'file_download_text' => 'Descargar', // default: "Download"
        'remove_text' => 'Remover', // default: "Remove"
    )
) );

$group_field_id = $cmb_home_product->add_field( array(
    'id'          => $prefix . 'home_products_group',
    'type'        => 'group',
    'description' => __( 'Beneficios', 'zunergy' ),
    'options'     => array(
        'group_title'       => __( 'Beneficio {#}', 'zunergy' ),
        'add_button'        => __( 'Agregar otro Beneficio', 'zunergy' ),
        'remove_button'     => __( 'Remover Beneficio', 'zunergy' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro que quiere eliminar este beneficio?', 'zunergy' )
    )
) );

$cmb_home_product->add_group_field( $group_field_id, array(
    'id'   => 'icon',
    'name' => esc_html__('Ícono del Beneficio', 'zunergy'),
    'desc' => esc_html__('Seleccione un icono descriptivo del beneficio.', 'zunergy'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Cargar Ícono', // default: "Add or Upload Files"
        'remove_image_text' => 'Remover Ícono', // default: "Remove Image"
        'file_text' => 'Ícono:', // default: "File:"
        'file_download_text' => 'Descargar', // default: "Download"
        'remove_text' => 'Remover', // default: "Remove"
    )
) );

$cmb_home_product->add_group_field( $group_field_id, array(
    'id'   => 'title',
    'name' => esc_html__('Título del Beneficio', 'zunergy'),
    'desc' => esc_html__("Ingrese un titulo descriptivo del beneficio", 'zunergy'),
    'type' => 'textarea_small'
) );

$cmb_home_product->add_group_field( $group_field_id, array(
    'id'   => 'description',
    'name' => esc_html__('Descripción del Beneficio', 'zunergy'),
    'desc' => esc_html__("Ingrese un texto descriptivo del beneficio", 'zunergy'),
    'type' => 'textarea_small'
) );

$cmb_home_product->add_field( array(
    'id'      => $prefix . 'home_products_subtitle',
    'name'      => esc_html__( 'Sub-Título de la Sección', 'zunergy' ),
    'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'zunergy' ),
    'type'    => 'wysiwyg',
    'options' => array(),
) );

/* --------------------------------------------------------------
4.- PRODUCT DESCRIPTION
-------------------------------------------------------------- */
$cmb_home_faq = new_cmb2_box( array(
    'id'            => $prefix . 'home_faq',
    'title'         => esc_html__( 'Inicio: Preguntas Frecuentes', 'zunergy' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => true
) );

$cmb_home_faq->add_field( array(
    'id'   => $prefix . 'home_faq_image',
    'name' => esc_html__('Imagen de Fondo', 'zunergy'),
    'desc' => esc_html__('Seleccione una imagen de fondo.', 'zunergy'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Cargar Imagen', // default: "Add or Upload Files"
        'remove_image_text' => 'Remover Imagen', // default: "Remove Image"
        'file_text' => 'Imagen:', // default: "File:"
        'file_download_text' => 'Descargar', // default: "Download"
        'remove_text' => 'Remover', // default: "Remove"
    )
) );