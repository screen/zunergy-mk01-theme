<?php
/* --------------------------------------------------------------
1.- NOSOTROS: BANNER
-------------------------------------------------------------- */
$cmb_nosotros_banner = new_cmb2_box( array(
    'id'            => $prefix . 'nosotros_banner',
    'title'         => esc_html__( 'Nosotros: Sección del Banner', 'zunergy' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'slug', 'value' => array('nosotros', 'our-company') ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => true
) );

$cmb_nosotros_banner->add_field( array(
    'id'      => $prefix . 'nosotros_banner_text',
    'name'      => esc_html__( 'Texto del Banner', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí el texto para el banner', 'zunergy' ),
    'type'    => 'wysiwyg',
    'options' => array(),
) );

$cmb_nosotros_banner->add_field( array(
    'id'   => $prefix . 'nosotros_banner_image',
    'name' => esc_html__('Imagen del Banner', 'zunergy'),
    'desc' => esc_html__('Seleccione una imagen para el banner.', 'zunergy'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Cargar Imagen', // default: "Add or Upload Files"
        'remove_image_text' => 'Remover Imagen', // default: "Remove Image"
        'file_text' => 'Imagen:', // default: "File:"
        'file_download_text' => 'Descargar', // default: "Download"
        'remove_text' => 'Remover', // default: "Remove"
    )
) );

/* --------------------------------------------------------------
2.- NOSOTROS: MISION / VISION
-------------------------------------------------------------- */
$cmb_nosotros_subitem_banner = new_cmb2_box( array(
    'id'            => $prefix . 'nosotros_subsection',
    'title'         => esc_html__( 'Nosotros: Misión / Vision', 'zunergy' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'slug', 'value' => array('nosotros', 'our-company') ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => true
) );

$cmb_nosotros_subitem_banner->add_field( array(
    'id'      => $prefix . 'nosotros_subsection_text',
    'name'      => esc_html__( 'Texto del Banner', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí el texto para el banner', 'zunergy' ),
    'type'    => 'wysiwyg',
    'options' => array(),
) );

$cmb_nosotros_subitem_banner->add_field( array(
    'id'   => $prefix . 'nosotros_subsection_image',
    'name' => esc_html__('Imagen del Banner', 'zunergy'),
    'desc' => esc_html__('Seleccione una imagen para el banner.', 'zunergy'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Cargar Imagen', // default: "Add or Upload Files"
        'remove_image_text' => 'Remover Imagen', // default: "Remove Image"
        'file_text' => 'Imagen:', // default: "File:"
        'file_download_text' => 'Descargar', // default: "Download"
        'remove_text' => 'Remover', // default: "Remove"
    )
) );
