<?php

/* GET SHOP CATEGORY */
function get_product_elements() {
    $products_array = array();
    $return_categories = array();
    $products_array = new WP_Query(array('post_type' => 'productos', 'posts_per_page' => -1));
    if ($products_array->have_posts() ) : 
    while ($products_array->have_posts() ) : $products_array->the_post();
    $return_categories[get_the_ID()] = get_the_title();
    endwhile;
    endif;
    wp_reset_query();

    return $return_categories;
}
/* --------------------------------------------------------------
1.- PRODUCTO: ESTIMADOR
-------------------------------------------------------------- */
$cmb_producto_estimador = new_cmb2_box( array(
    'id'            => $prefix . 'producto_estimador_metabox',
    'title'         => esc_html__( 'Producto: Sección de Estimador', 'zunergy' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'slug', 'value' => array('productos', 'products') ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => true
) );

$cmb_producto_estimador->add_field( array(
    'id'      => $prefix . 'product_title',
    'name'      => esc_html__( 'Título del Producto', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí un texto descriptivo que hará de título justo encima del producto', 'zunergy' ),
    'type'    => 'wysiwyg'
) );

$cmb_producto_estimador->add_field( array(
    'id'        => $prefix . 'product_selected',
    'name'      => esc_html__( 'Seleccione Producto', 'zunergy' ),
    'desc'      => esc_html__( 'Seleccione el producto que el erstimador usara para calcular', 'zunergy' ),
    'type'      => 'select',
    'show_option_none' => true,
    'default'   => 'custom',
    'options'   => get_product_elements()
) );

$cmb_producto_estimador->add_field( array(
    'id'   => $prefix . 'product_contact_image',
    'name' => esc_html__('Imágen para el area de Contacto', 'zunergy'),
    'desc' => esc_html__('Seleccione Imágen para el area de Contacto.', 'zunergy'),
    'type' => 'file',
    'preview_size' => array( 100, 100 ),
    'query_args' => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text' => 'Cargar Imagen', // default: "Add or Upload Files"
        'remove_image_text' => 'Remover Imagen', // default: "Remove Image"
        'file_text' => 'Imagen:', // default: "File:"
        'file_download_text' => 'Descargar', // default: "Download"
        'remove_text' => 'Remover', // default: "Remove"
    )
) );