<?php
function be_metabox_show_on_slug( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
        return $display;
    }

    if ( 'slug' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return $display;
    }

    $slug = get_post( $post_id )->post_name;

    // See if there's a match
    return in_array( $slug, (array) $meta_box['show_on']['value']);
}

add_filter( 'cmb2_show_on', 'be_metabox_show_on_slug', 10, 2 );


function ed_metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
        return $display;
    }

    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );

add_action( 'cmb2_admin_init', 'zunergy_register_custom_metabox' );
function zunergy_register_custom_metabox() {
    $prefix = 'zgy_';

    /* --------------------------------------------------------------
    0.- PAGE: MAIN TITLE
    -------------------------------------------------------------- */
    $cmb_nosotros_title = new_cmb2_box( array(
        'id'            => $prefix . 'main_title',
        'title'         => esc_html__( 'Página: Título principal', 'zunergy' ),
        'object_types'  => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_nosotros_title->add_field( array(
        'id'      => $prefix . 'main_title_text',
        'name'      => esc_html__( 'Título principal', 'zunergy' ),
        'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'zunergy' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    /* --------------------------------------------------------------
    1.- PAGE: ESCALADOR
    -------------------------------------------------------------- */
    $cmb_escalador = new_cmb2_box( array(
        'id'            => $prefix . 'escalador_metabox',
        'title'         => esc_html__( 'Página: Escalador', 'zunergy' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'slug', 'value' => array('escalador', 'escalator') ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $group_field_id = $cmb_escalador->add_field( array(
        'id'          => $prefix . 'product_scale_basic_group',
        'type'        => 'group',
        'description' => __( 'Escala: Basica', 'zunergy' ),
        'options'     => array(
            'group_title'       => __( 'Item {#}', 'zunergy' ),
            'add_button'        => __( 'Agregar otro Item', 'zunergy' ),
            'remove_button'     => __( 'Remover Item', 'zunergy' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( '¿Esta seguro que quiere eliminar este Item?', 'zunergy' )
        )
    ) );

    $cmb_escalador->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name' => esc_html__('Título del Item', 'zunergy'),
        'desc' => esc_html__("Ingrese un texto descriptivo del Item", 'zunergy'),
        'type' => 'text'
    ) );


    $cmb_escalador->add_group_field( $group_field_id, array(
        'id'   => 'value',
        'name' => esc_html__('Valor', 'zunergy'),
        'desc' => esc_html__("Ingrese un texto descriptivo del Item", 'zunergy'),
        'type' => 'text_small'
    ) );

    $group_field_id = $cmb_escalador->add_field( array(
        'id'          => $prefix . 'product_scale_medium_group',
        'type'        => 'group',
        'description' => __( 'Escala: Media', 'zunergy' ),
        'options'     => array(
            'group_title'       => __( 'Item {#}', 'zunergy' ),
            'add_button'        => __( 'Agregar otro Item', 'zunergy' ),
            'remove_button'     => __( 'Remover Item', 'zunergy' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( '¿Esta seguro que quiere eliminar este Item?', 'zunergy' )
        )
    ) );

    $cmb_escalador->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name' => esc_html__('Título del Item', 'zunergy'),
        'desc' => esc_html__("Ingrese un texto descriptivo del Item", 'zunergy'),
        'type' => 'text'
    ) );


    $cmb_escalador->add_group_field( $group_field_id, array(
        'id'   => 'value',
        'name' => esc_html__('Valor', 'zunergy'),
        'desc' => esc_html__("Ingrese un texto descriptivo del Item", 'zunergy'),
        'type' => 'text_small'
    ) );

    $group_field_id = $cmb_escalador->add_field( array(
        'id'          => $prefix . 'product_scale_high_group',
        'type'        => 'group',
        'description' => __( 'Escala: Alta', 'zunergy' ),
        'options'     => array(
            'group_title'       => __( 'Item {#}', 'zunergy' ),
            'add_button'        => __( 'Agregar otro Item', 'zunergy' ),
            'remove_button'     => __( 'Remover Item', 'zunergy' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( '¿Esta seguro que quiere eliminar este Item?', 'zunergy' )
        )
    ) );

    $cmb_escalador->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name' => esc_html__('Título del Item', 'zunergy'),
        'desc' => esc_html__("Ingrese un texto descriptivo del Item", 'zunergy'),
        'type' => 'text'
    ) );


    $cmb_escalador->add_group_field( $group_field_id, array(
        'id'   => 'value',
        'name' => esc_html__('Valor', 'zunergy'),
        'desc' => esc_html__("Ingrese un texto descriptivo del Item", 'zunergy'),
        'type' => 'text_small'
    ) );


    /* HOME CUSTOM METABOXES */
    include_once('custom_home_metaboxes.php');
    /* NOSOTROS CUSTOM METABOXES */
    include_once('custom_nosotros_metaboxes.php');
    /* PRODUCTOS CUSTOM METABOXES */
    include_once('custom_productos_metaboxes.php');
    /* CONTACTO CUSTOM METABOXES */
    include_once('custom_contacto_metaboxes.php');
    /* PRODUCTOS CUSTOM METABOXES */
    include_once('custom_productos_type_metaboxes.php');
    /* DISTRIBUIDORES CUSTOM METABOXES */
    include_once('custom_distribuidores_metaboxes.php');

}
