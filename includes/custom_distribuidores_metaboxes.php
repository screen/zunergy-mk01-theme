<?php
/* --------------------------------------------------------------
1.- DISTRIBUIDORES
-------------------------------------------------------------- */
$cmb_contacto_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'dist_info',
    'title'         => esc_html__( 'Distribuidores: Información Adicional', 'zunergy' ),
    'object_types'  => array( 'distribuidores' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_contacto_metabox->add_field( array(
    'id'      => $prefix . 'dist_telf',
    'name'      => esc_html__( 'Teléfono', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí el teléfono', 'zunergy' ),
    'type'    => 'text'
) );

$cmb_contacto_metabox->add_field( array(
    'id'      => $prefix . 'dist_email',
    'name'      => esc_html__( 'Correo Electrónico', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí el Correo Electrónico', 'zunergy' ),
    'type'    => 'text_email'
) );
