<?php
/* --------------------------------------------------------------
2.- PRODUCTO: INFORMACION
-------------------------------------------------------------- */
$cmb_producto_info = new_cmb2_box( array(
    'id'            => $prefix . 'product_info_metabox',
    'title'         => esc_html__( 'Producto: Info', 'zunergy' ),
    'object_types'  => array( 'productos' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_producto_info->add_field( array(
    'id'      => $prefix . 'product_capacity',
    'name'      => esc_html__( 'Capacidad del Producto', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí un texto descriptivo que describa el producto', 'zunergy' ),
    'type'    => 'textarea_small'
) );

$cmb_producto_info->add_field( array(
    'id'      => $prefix . 'product_capacity_value',
    'name'      => esc_html__( 'Capacidad del Producto (Solo Valor en Watts)', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí un texto descriptivo que describa el producto', 'zunergy' ),
    'type'    => 'text_small'
) );

$cmb_producto_info->add_field( array(
    'id'      => $prefix . 'product_info',
    'name'      => esc_html__( 'Información del Producto', 'zunergy' ),
    'desc'      => esc_html__( 'Ingrese aquí un texto descriptivo que describa el producto', 'zunergy' ),
    'type'    => 'wysiwyg'
) );

/* --------------------------------------------------------------
2.- PRODUCTO: ESPECIFICACIONES
-------------------------------------------------------------- */

$cmb_producto_specs = new_cmb2_box( array(
    'id'            => $prefix . 'product_features',
    'title'         => esc_html__( 'Producto: Especificaciones', 'zunergy' ),
    'object_types'  => array( 'productos' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$group_field_id = $cmb_producto_specs->add_field( array(
    'id'          => $prefix . 'product_specs_group',
    'type'        => 'group',
    'description' => __( 'Especificaciones', 'zunergy' ),
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'zunergy' ),
        'add_button'        => __( 'Agregar otro Item', 'zunergy' ),
        'remove_button'     => __( 'Remover Item', 'zunergy' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro que quiere eliminar este Item?', 'zunergy' )
    )
) );

$cmb_producto_specs->add_group_field( $group_field_id, array(
    'id'   => 'title',
    'name' => esc_html__('Título del Item', 'zunergy'),
    'desc' => esc_html__("Ingrese un texto descriptivo del Item", 'zunergy'),
    'type' => 'text'
) );


$cmb_producto_specs->add_group_field( $group_field_id, array(
    'id'   => 'description',
    'name' => esc_html__('Descripción del Item', 'zunergy'),
    'desc' => esc_html__("Ingrese un texto descriptivo del Item", 'zunergy'),
    'type' => 'textarea_small'
) );

