<?php get_header(); ?>
<?php $page_products = get_page_by_path('productos'); ?>
<?php $page_escalador = get_page_by_path('escalador'); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="main-product-banner col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="main-product-banner-text col-xl-5 col-lg-5 col-md-6 col-sm-7 col-12">
                        <?php $content = get_post_meta($page_products->ID, 'zgy_main_title_text', true ); ?>
                        <h1 class="main-title-section"><?php echo apply_filters('the_content', $content); ?></h1>
                        <?php echo apply_filters('the_content', $page_products->post_content); ?>
                    </div>
                    <div class="main-product-banner-image col-xl-6 offset-xl-1 col-lg-6 offset-lg-1 col-md-6 col-sm-12 col-12">
                        <?php echo get_the_post_thumbnail($page_products, 'full', array('class' => 'img-fluid')); ?>
                    </div>
                </div>
            </div>
            <div class="hexagon-container"></div>
        </section>
        <section class="main-product-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="main-product-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $content = get_post_meta($page_products->ID, 'zgy_product_title', true ); ?>
                        <h2 class="main-product-title"><?php echo apply_filters('the_content', $content); ?></h2>
                        <div class="row">
                            <div class="main-product-nav-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <ul class="nav nav-tabs justify-content-center" id="nav_product" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="consumo-tab" data-toggle="tab" href="#consumo" role="tab" aria-controls="consumo" aria-selected="true" title="<?php _e('Haga click aquí para iniciar el Estimador de Consumo', 'zunergy'); ?>"><?php _e('¿Cuánta capacidad requiero?', 'zunergy'); ?></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="especificaciones-tab" data-toggle="tab" href="#especificaciones" role="tab" aria-controls="especificaciones" aria-selected="false" title="<?php _e('Haga click aquí para ver las Especificaciones', 'zunergy'); ?>"><?php _e('Especificaciones', 'zunergy'); ?></a>
                                    </li>
                                </ul>
                                <?php $product_selected = get_post_meta($page_products->ID, 'zgy_product_selected', true ); ?>
                                <?php $product_object = get_post($product_selected); ?>
                                <div class="tab-content" id="nav_product_content">
                                    <div class="tab-pane fade show active" id="consumo" role="tabpanel" aria-labelledby="consumo-tab">
                                        <div class="row">
                                            <div class="product-estimate-overview col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="row align-items-center justify-content-between">
                                                    <div class="product-estimate-item product-estimate-item-left col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
                                                        <h3><?php _e('Nuestro estimador te ayudará a saber cuántas horas de energía puedes almacenar', 'zunergy'); ?></h3>
                                                    </div>
                                                    <div class="product-estimate-item col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                                                        <?php echo get_the_post_thumbnail($product_selected, 'full', array('class' => 'img-fluid')); ?>
                                                        <p class="text-center"><?php _e('Esta medida aplica para una configuración standard de xx', 'zunergy'); ?></p>
                                                    </div>
                                                    <div class="product-estimate-item product-estimate-item-right col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
                                                        <?php $content = get_post_meta($product_selected, 'zgy_product_info', true); ?>
                                                        <?php echo apply_filters('the_content', $content); ?>
                                                        <a href="#" class="btn btn-md btn-product-learn-more" title="<?php _e('Haga click aquí para ver mas información', 'zunergy'); ?>"><?php _e('Leer Más', 'zunergy'); ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-estimate-controller col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="row">
                                                    <div class="product-estimate-controller-buttons col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <input type="hidden" name="quantity" value="1" />
                                                        <button class="btn-minus" title="<?php _e('Haga click aquí para disminuir la cantidad de Equipos Delta para el cálculo', 'zunergy'); ?>">-</button> <span title="<?php _e('Cantidad de Equipos Delta seleccionados para el cálculo', 'zunergy'); ?>">1</span> <button class="btn-plus" title="<?php _e('Haga click aquí para aumentar la cantidad de Equipos Delta para el cálculo', 'zunergy'); ?>">+</button>
                                                    </div>
                                                    <div class="product-estimate-tips col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <h4><?php echo sprintf('Zunergy recomienda  <strong>al menos dos equipos Delta</strong>', 'zunergy'); ?>.</h4>
                                                        <h2><?php echo sprintf('Estimado para una casa de <strong>3 habitaciones</strong> con equipos eficientes', 'zunergy'); ?>.</h2>
                                                    </div>
                                                </div>
                                            </div>

                                            <form name="form-estimator" class="product-scale-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <input type="hidden" name="product_watt" value="<?php echo get_post_meta($product_object->ID, 'zgy_product_capacity_value', true); ?>" />
                                                <input type="hidden" name="acum_watt" value="0" />
                                                <div class="row align-items-center justify-content-between">
                                                    <div class="product-scale-item col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
                                                        <div class="product-scale-item-title">
                                                            <h2><?php _e('Consumo Básico', 'zunergy'); ?></h2>
                                                            <h2>0 - 99W</h2>
                                                        </div>
                                                        <?php $basic_scale = get_post_meta($page_escalador->ID, 'zgy_product_scale_basic_group', true); ?>
                                                        <?php $i =1; ?>
                                                        <?php foreach ($basic_scale as $item) { ?>
                                                        <label for="basic_<?php echo $i; ?>" title="<?php echo sprintf( esc_html__( 'Equivalente a %s W de Potencia', 'zunergy' ), $item['value']); ?>"><input id="basic_<?php echo $i; ?>" type="checkbox" name="<?php echo $item['title']; ?>" value="<?php echo $item['value']; ?>" /> <?php echo $item['title']; ?></label>
                                                        <?php $i++; } ?>
                                                    </div>

                                                    <div class="product-scale-item col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
                                                        <div class="product-scale-item-title">
                                                            <h2><?php _e('Consumo Medio', 'zunergy'); ?></h2>
                                                            <h2>100 - 499W</h2>
                                                        </div>
                                                        <?php $medium_scale = get_post_meta($page_escalador->ID, 'zgy_product_scale_medium_group', true); ?>
                                                        <?php $i = 1; ?>
                                                        <?php foreach ($medium_scale as $item) { ?>
                                                        <label for="medium_<?php echo $i; ?>" title="<?php echo sprintf( esc_html__( 'Equivalente a %s W de Potencia', 'zunergy' ), $item['value']); ?>"><input id="medium_<?php echo $i; ?>" type="checkbox" name="<?php echo $item['title']; ?>" value="<?php echo $item['value']; ?>" /> <?php echo $item['title']; ?></label>
                                                        <?php $i++; } ?>
                                                    </div>

                                                    <div class="product-scale-item col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
                                                        <div class="product-scale-item-title">
                                                            <h2><?php _e('Consumo Alto', 'zunergy'); ?></h2>
                                                            <h2>500 - 2000W</h2>
                                                        </div>
                                                        <?php $high_scale = get_post_meta($page_escalador->ID, 'zgy_product_scale_high_group', true); ?>
                                                        <?php $i = 1; ?>
                                                        <?php foreach ($high_scale as $item) { ?>
                                                        <label for="high_<?php echo $i; ?>" title="<?php echo sprintf( esc_html__( 'Equivalente a %s W de Potencia', 'zunergy' ), $item['value']); ?>"><input id="high_<?php echo $i; ?>" type="checkbox" name="<?php echo $item['title']; ?>" value="<?php echo $item['value']; ?>" /> <?php echo $item['title']; ?></label>
                                                        <?php $i++; } ?>
                                                    </div>
                                                    <div class="w-100"></div>
                                                </div>
                                            </form>
                                            <div class="product-scale-results-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <h3><?php echo sprintf('con <span>01</span> Equipos Delta + Bateria de 7 kwh puedes almacenar energía hasta para', 'zunergy'); ?></h3>
                                                <h2><?php echo sprintf('<span>336</span> Horas de Energía Eléctrica', 'zunergy'); ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="especificaciones" role="tabpanel" aria-labelledby="especificaciones-tab">
                                        <div class="row">
                                            <?php $product_specs_group = get_post_meta($product_selected, 'zgy_product_specs_group', true); ?>
                                            <?php $quantity = count($product_specs_group); ?>
                                            <?php $middle = $quantity / 2; ?>
                                            <div class="product-specs-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="row align-items-center justify-content-between">
                                                    <div class="product-specs-item product-specs-item-left col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                                                        <div class="row">
                                                            <?php $i = 1; ?>
                                                            <?php foreach ($product_specs_group as $specs_item) { ?>
                                                            <?php if ($i <= $middle) { ?>
                                                            <div class="product-specs-group-item col-12">
                                                                <h2><?php echo $specs_item['title']; ?></h2>
                                                                <?php echo apply_filters('the_content', $specs_item['description']); ?>
                                                            </div>
                                                            <?php } ?>
                                                            <?php $i++; } ?>
                                                        </div>
                                                    </div>
                                                    <div class="product-specs-item col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                                                        <?php echo get_the_post_thumbnail($product_selected, 'full', array('class' => 'img-fluid')); ?>
                                                    </div>
                                                    <div class="product-specs-item product-specs-item-right col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                                                        <div class="row">
                                                            <?php $i = 1; ?>
                                                            <?php foreach ($product_specs_group as $specs_item) { ?>
                                                            <?php if ($i > $middle) { ?>
                                                            <div class="product-specs-group-item col-12">
                                                                <h2><?php echo $specs_item['title']; ?></h2>
                                                                <?php echo apply_filters('the_content', $specs_item['description']); ?>
                                                            </div>
                                                            <?php } ?>
                                                            <?php $i++; } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-product-form-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-end justify-content-end">
                    <div class="main-product-form-title col-xl-5 col-lg-5 col-md-5 col-sm-5 col-12">
                        <h2><?php echo sprintf('¡No esperes más por tu <strong>Cotización!</strong>', 'zunergy'); ?></h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-product-form-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="hexagon-container"></div>
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="main-product-form-image col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php $image_id = get_post_meta($page_products->ID, 'zgy_product_contact_image_id', true); ?>
                        <?php echo wp_get_attachment_image($image_id, 'full', false, array('class' => 'img-fluid')); ?>
                    </div>
                    <div class="main-product-form-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="custom-contact-form-control col-12">
                            <label for="nombre"><?php _e('Nombre', 'zunergy')?> <span class="required">*</span>
                                <input type="text" name="nombre" class="form-control" required />
                            </label>
                        </div>
                        <div class="custom-contact-form-control col-12">
                            <label for="correo"><?php _e('Correo Electrónico', 'zunergy')?> <span class="required">*</span>
                                <input type="text" name="correo" class="form-control" required />
                            </label>
                        </div>
                        <div class="custom-contact-form-control col-12">
                            <label for="telefono"><?php _e('Número de teléfono', 'zunergy')?> <span class="required">*</span>
                                <input type="text" name="telefono" class="form-control" required />
                            </label>
                        </div>
                        <div class="custom-contact-form-control col-12">
                            <button type="submit" class="btn btn-md btn-contact-submit" title="<?php _e('Haga click aquí para obtener su cotización', 'zunergy'); ?>"><?php _e('¡Obtenlo Ahora!', 'zunergy'); ?></button>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <section class="main-product-more-info col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="main-product-more-info-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php _e('¿Necesitas más ayuda con tu cotización?', 'zunergy'); ?></h2>
                        <a href="" target="_blank" title="<?php _e('Recibe más ayuda directamente a tu WhatsApp', 'zunergy'); ?>" class="btn btn-md btn-whatsapp"><i class="fa fa-whatsapp"></i> <?php _e('Recibe Asesoría', 'zunergy'); ?></a>
                    </div>
                </div>
            </div>

        </section>
    </div>
</main>
<?php get_footer(); ?>
