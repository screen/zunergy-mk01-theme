<?php get_header(); ?>
<?php the_post(); ?>
<main id="post-<?php the_ID(); ?>" class="container-fluid p-0">
    <div class="row no-gutters">
        <?php /* SLIDER PRINCIPAL */?>
        <?php $bg_image = get_post_meta(get_the_ID(), 'zgy_hero_background', true); ?>
        <section class="the-hero col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_image; ?>);">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="hero-item hero-text col-xl-6 col-lg-8 col-md-9 col-sm-6 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'zgy_hero_title', true); ?></h2>
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'zgy_hero_description', true)); ?>
                        <a href="<?php echo home_url('/nosotros'); ?>" title="<?php _e('Haz click aquí para conocer más', 'zunergy'); ?>" class="btn btn-md btn-hero"><?php _e('Conoce más', 'zunergy'); ?></a>
                    </div>
                    <div class="hero-item hero-image col-xl-4 col-lg-4 col-md-3 col-sm-6 col-6 d-xl-flex d-lg-flex d-md-flex d-sm-flex d-none">
                        <?php $hero_image = get_post_meta(get_the_ID(), 'zgy_hero_image', true); ?>
                        <img src="<?php echo $hero_image; ?>" alt="Hero image" class="img-fluid" />
                    </div>
                </div>
            </div>
        </section>
        <section class="the-features col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="features-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="features-container-wrapper">
                            <h2><?php echo get_post_meta(get_the_ID(), 'zgy_features_title', true); ?></h2>
                            <div class="row align-items-start justify-content-center">
                                <?php $features_array = get_post_meta(get_the_ID(), 'zgy_home_features_group', true); ?>
                                <?php if ((!empty($features_array)) || ($features_array != '' )) { ?>
                                <?php foreach ($features_array as $features_item) { ?>
                                <div class="features-item col-xl col-lg col-md-4 col-sm-6 col-6">
                                    <img src="<?php echo $features_item['icon']; ?>" alt="" class="img-fluid" />
                                    <h3><?php echo $features_item['description']; ?></h3>
                                </div>
                                <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="hexagon-container"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="explainer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="explainer-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'zgy_home_exp_title', true); ?></h2>
                        <div class="explainer-content">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'zgy_home_exp_desc', true)); ?>
                        </div>
                        <?php $hero_image = get_post_meta(get_the_ID(), 'zgy_home_exp_image', true); ?>
                        <img src="<?php echo $hero_image; ?>" alt="Hero image" class="img-fluid" />
                    </div>
                </div>
            </div>
        </section>
        <section class="the-product col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">

                    <div class="product-left-bar col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 d-xl-flex d-lg-flex d-md-flex d-sm-none d-none">
                        <div class="row">
                            <?php $features_array = get_post_meta(get_the_ID(), 'zgy_home_products_group', true); ?>
                            <?php $i = 1; ?>
                            <?php if ((!empty($features_array)) || ($features_array != '' )) { ?>
                            <?php foreach ($features_array as $features_item) { ?>
                            <div class="product-overview-item col-12">
                                <img src="<?php echo $features_item['icon']; ?>" alt="" class="img-fluid" />
                                <h3><?php echo $features_item['title']; ?></h3>
                                <?php echo apply_filters('the_content',  $features_item['description']); ?>
                            </div>
                            <?php if ($i == 3) { break; } ?>
                            <?php $i++; } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="product-image-bar col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <h2><?php echo get_post_meta(get_the_ID(), 'zgy_home_products_title', true); ?></h2>
                        <?php $hero_image = get_post_meta(get_the_ID(), 'zgy_home_product_image', true); ?>
                        <img src="<?php echo $hero_image; ?>" alt="Hero image" class="img-fluid" />

                    </div>
                    <div class="product-right-bar col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 d-xl-flex d-lg-flex d-md-flex d-sm-none d-none">
                        <div class="row">
                            <?php $i = 1; ?>
                            <?php if ((!empty($features_array)) || ($features_array != '' )) { ?>
                            <?php foreach ($features_array as $features_item) { ?>
                            <?php if ($i > 3) { ?>
                            <div class="product-overview-item col-xl-12 col-lg-12 col-md-12 col-sm-4 col-6">
                                <img src="<?php echo $features_item['icon']; ?>" alt="" class="img-fluid" />
                                <h3><?php echo $features_item['title']; ?></h3>
                                <?php echo apply_filters('the_content',  $features_item['description']); ?>
                            </div>
                            <?php } ?>
                            <?php $i++; } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="product-center-bar col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-xl-none d-lg-none d-md-none d-sm-block d-block">
                        <h3><?php echo get_post_meta(get_the_ID(), 'zgy_home_products_subtitle', true); ?></h3>
                        <a href="" class="btn btn-md btn-home-product" title="<?php _e('¡Encuentra el equipo ideal!', 'zunergy'); ?>"><?php _e('¡Encuentra el equipo ideal!', 'zunergy'); ?></a>
                    </div>
                    <div class="product-left-bar col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 d-xl-none d-lg-none d-md-none d-sm-flex d-flex">
                        <div class="row">
                            <?php $features_array = get_post_meta(get_the_ID(), 'zgy_home_products_group', true); ?>

                            <?php if ((!empty($features_array)) || ($features_array != '' )) { ?>
                            <?php foreach ($features_array as $features_item) { ?>
                            <div class="product-overview-item col-xl-12 col-lg-12 col-md-12 col-sm-4 col-6">
                                <img src="<?php echo $features_item['icon']; ?>" alt="" class="img-fluid" />
                                <h3><?php echo $features_item['title']; ?></h3>
                                <?php echo apply_filters('the_content',  $features_item['description']); ?>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="product-center-bar col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-xl-block d-lg-block d-md-block d-sm-none d-none">
                        <h3><?php echo get_post_meta(get_the_ID(), 'zgy_home_products_subtitle', true); ?></h3>
                        <a href="<?php echo home_url('/productos'); ?>" class="btn btn-md btn-home-product" title="<?php _e('¡Encuentra el equipo ideal!', 'zunergy'); ?>"><?php _e('¡Encuentra el equipo ideal!', 'zunergy'); ?></a>
                    </div>
                </div>
            </div>
        </section>
        <?php $faq_image = get_post_meta(get_the_ID(), 'zgy_home_faq_image', true); ?>
        <section class="the-faq col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $faq_image; ?>);">
            <div class="container">
                <div class="row">
                    <div class="home-faq-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php _e('Preguntas Frecuentes', 'zunergy'); ?></h2>
                        <div class="row align-items-center justify-content-center">
                            <div class="home-faq-content col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12">
                                <div class="accordion custom-accordion" id="faq_collapse">
                                    <?php $array_faq = new WP_Query(array('post_type' => 'faq', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date')); ?>
                                    <?php if ($array_faq->have_posts()) : ?>
                                    <?php while ($array_faq->have_posts()) : $array_faq->the_post(); ?>
                                    <div class="card">
                                        <div class="card-header" id="heading<?php echo get_the_ID(); ?>">
                                            <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapse<?php echo get_the_ID(); ?>" aria-expanded="true" aria-controls="collapse<?php echo get_the_ID(); ?>">
                                                <?php the_title(); ?>
                                                <div class="plus">
                                                    <span></span>
                                                    <span></span>
                                                </div>
                                            </h2>

                                        </div>

                                        <div id="collapse<?php echo get_the_ID(); ?>" class="collapse" aria-labelledby="heading<?php echo get_the_ID(); ?>" data-parent="#faq_collapse">
                                            <div class="card-body">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                    <?php wp_reset_query(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
