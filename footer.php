<?php $footer_options = get_option('zunergy_footer_settings'); ?>
<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $footer_options['footer_bg']; ?>); ">
            <div class="container">
                <div class="row align-items-start">
                    <div class="footer-item col-xl col-lg col-md col-sm-12 col-12">
                        <?php if ( is_active_sidebar( 'sidebar_footer' ) ) : ?>
                        <ul id="sidebar-footer1">
                            <?php dynamic_sidebar( 'sidebar_footer' ); ?>
                        </ul>
                        <?php endif; ?>
                    </div>
                    <div class="footer-item col-xl col-lg col-md col-sm-12 col-12">
                        <?php if ( is_active_sidebar( 'sidebar_footer-2' ) ) : ?>
                        <ul id="sidebar-footer2">
                            <?php dynamic_sidebar( 'sidebar_footer-2' ); ?>
                        </ul>
                        <?php endif; ?>
                    </div>
                    <div class="footer-item col-xl col-lg col-md col-sm-12 col-12">
                        <?php if ( is_active_sidebar( 'sidebar_footer-3' ) ) : ?>
                        <ul id="sidebar-footer3">
                            <?php dynamic_sidebar( 'sidebar_footer-3' ); ?>
                        </ul>
                        <?php endif; ?>
                    </div>
                    <div class="w-100"></div>
                    <div class="footer-copy col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h5>Copyright &copy; 2019 - <?php printf( __('DEVELOPED BY <a href="%s">SMG | DIGITAL MARKETING AGENCY</a>', 'zunergy'), 'http://screen.com.ve'); ?></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>

</html>
