var quantity = jQuery('input[name=quantity]').val(), acum = 0;
jQuery(document).ready(function ($) {
    "use strict";

    jQuery('.btn-minus').on('click', function (e) {
        e.stopPropagation();
        if (quantity > 1) {
            quantity = parseInt(quantity) - 1;
        } else {
            quantity = 1;
        }
        jQuery('input[name=quantity]').val(quantity);
        jQuery('.product-estimate-controller-buttons span').html(quantity);
        jQuery('.product-scale-results-container h3 span').html(pad(quantity, 2));
        calculateEnergy(acum);
    });

    jQuery('.btn-plus').on('click', function (e) {
        e.stopPropagation();
        quantity = parseInt(quantity) + 1;
        jQuery('input[name=quantity]').val(quantity);
        jQuery('.product-estimate-controller-buttons span').html(quantity);
        jQuery('.product-scale-results-container h3 span').html(pad(quantity, 2));
        calculateEnergy(acum);
    });


    jQuery('input[type=checkbox]').on('change', function (e) {
        if (jQuery(this).is(':checked')) {
            acum = acum + parseInt(jQuery(this).val());
        } else {
            acum = acum - parseInt(jQuery(this).val());
        }
        calculateEnergy(acum);
    });
}); /* end of as page load scripts */

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function calculateEnergy(consumpt) {
    if (consumpt > 0) {
        var product_charge = parseInt(jQuery('input[name=product_watt]').val()) * parseInt(jQuery('input[name=quantity]').val());
        var hour_time = product_charge / consumpt;
        jQuery('.product-scale-results-container h2 span').html(parseInt(hour_time));
    }
}
