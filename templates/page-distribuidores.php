<?php
/**
* Template Name: Template - Distribuidores
*
* @package zunergy
* @subpackage zunergy-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="main-distribuidores-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="main-distribuidores-content col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                        <?php $content = get_post_meta(get_the_ID(), 'zgy_main_title_text', true ); ?>
                        <h1 class="main-title-section"><?php echo apply_filters('the_content', $content); ?></h1>
                        <?php the_content(); ?>
                    </div>
                    <div class="main-distribuidores-content col-xl-6 offset-xl-1 col-lg-6 offset-lg-1 col-md-6 col-sm-12 col-12">
                        <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="distribuidores-contact-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="distribuidores-content col-xl-12 col-lg-11 col-md-11 col-sm-12 col-12">
                        <div class="row">
                            <div class="custom-contact-form-control col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <label for="nombre"><?php _e('Nombre', 'zunergy')?> <span class="required">*</span>
                                    <input type="text" name="nombre" class="form-control" required />
                                </label>
                            </div>
                            <div class="custom-contact-form-control col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <label for="apellido"><?php _e('Apellido', 'zunergy')?> <span class="required">*</span>
                                    <input type="text" name="apellido" class="form-control" required />
                                </label>
                            </div>
                            <div class="custom-contact-form-control col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <label for="telefono"><?php _e('Número de teléfono', 'zunergy')?> <span class="required">*</span>
                                    <input type="text" name="telefono" class="form-control" required />
                                </label>
                            </div>
                            <div class="custom-contact-form-control col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <label for="correo"><?php _e('Correo Electrónico', 'zunergy')?> <span class="required">*</span>
                                    <input type="text" name="correo" class="form-control" required />
                                </label>
                            </div>
                            <div class="custom-contact-form-control col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <label for="empresa"><?php _e('Empresa', 'zunergy')?> <span class="required">*</span>
                                    <input type="text" name="empresa" class="form-control" required />
                                </label>
                            </div>
                            <div class="custom-contact-form-control col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <label for="pais"><?php _e('País', 'zunergy')?> <span class="required">*</span>
                                    <select name="pais" id="" class="form-control" title="<?php _e('Seleccione el país de origen', 'zunergy'); ?>">
                                        <option value=""></option>
                                    </select>
                                </label>
                            </div>
                            <div class="custom-contact-form-control col-12">
                                <label for="comments"><?php _e('Comentarios', 'zunergy')?>
                                    <textarea name="comments" id="" cols="30" rows="4" class="form-control" title="<?php _e('Complete este campo, por favor sea descriptivo', 'zunergy'); ?>"></textarea>
                                </label>
                            </div>
                            <div class="custom-contact-form-control col-12">
                                <button type="submit" class="btn btn-md btn-submit" title="<?php _e('Haga click aquí para enviar su mensaje', 'zunergy'); ?>"><?php _e('Enviar', 'zunergy'); ?></button>
                            </div>
                        </div>
                        <div class="hexagon-container"></div>
                        <div class="hexagon-container second-hexagon"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
