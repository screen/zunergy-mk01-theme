<?php
/**
* Template Name: Template - Nosotros
*
* @package zunergy
* @subpackage zunergy-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="main-nosotros-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="main-nosotros-content col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                        <?php $content = get_post_meta(get_the_ID(), 'zgy_main_title_text', true ); ?>
                        <h1 class="main-title-section"><?php echo apply_filters('the_content', $content); ?></h1>
                        <?php the_content(); ?>
                    </div>
                    <div class="main-nosotros-content col-xl-6 offset-xl-1 col-lg-6 offset-lg-1 col-md-12 col-sm-12 col-12">
                        <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-nosotros-full-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters align-items-center">
                    <div class="main-nosotros-full-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="main-nosotros-full-content-wrapper">
                            <?php $content = get_post_meta(get_the_ID(), 'zgy_nosotros_banner_text', true ); ?>
                            <?php echo apply_filters('the_content', $content); ?>
                        </div>
                    </div>
                    <div class="main-nosotros-full-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php $attachment_id = get_post_meta(get_the_ID(), 'zgy_nosotros_banner_image_id', true); ?>
                        <?php echo wp_get_attachment_image( $attachment_id, 'full', false, array('class' => 'img-fluid')); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-nosotros-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="main-nosotros-content col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                        <?php $attachment_id = get_post_meta(get_the_ID(), 'zgy_nosotros_subsection_image_id', true); ?>
                        <?php echo wp_get_attachment_image( $attachment_id, 'full', false, array('class' => 'img-fluid')); ?>
                    </div>
                    <div class="main-nosotros-content col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                        <?php $content = get_post_meta(get_the_ID(), 'zgy_nosotros_subsection_text', true ); ?>
                        <?php echo apply_filters('the_content', $content); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
