<?php
/**
* Template Name: Template - Preguntas Frecuentes
*
* @package zunergy
* @subpackage zunergy-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="main-preguntas-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="main-preguntas-content col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                        <?php $content = get_post_meta(get_the_ID(), 'zgy_main_title_text', true ); ?>
                        <h1 class="main-title-section"><?php echo apply_filters('the_content', $content); ?></h1>
                        <?php the_content(); ?>
                    </div>
                    <div class="main-preguntas-content col-xl-6 offset-xl-1 col-lg-6 offset-lg-1 col-md-6 col-sm-12 col-12">
                        <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-preguntas-collapse-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="main-preguntas-collapse-content col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">
                        <div class="hexagon-container"></div>
                        <div class="hexagon-container second-hexagon"></div>
                        <div class="accordion custom-accordion" id="faq_collapse">
                            <?php $array_faq = new WP_Query(array('post_type' => 'faq', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date')); ?>
                            <?php if ($array_faq->have_posts()) : ?>
                            <?php while ($array_faq->have_posts()) : $array_faq->the_post(); ?>
                            <div class="card">
                                <div class="card-header" id="heading<?php echo get_the_ID(); ?>">
                                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapse<?php echo get_the_ID(); ?>" aria-expanded="true" aria-controls="collapse<?php echo get_the_ID(); ?>">
                                        <?php the_title(); ?>
                                        <div class="plus">
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </h2>

                                </div>

                                <div id="collapse<?php echo get_the_ID(); ?>" class="collapse" aria-labelledby="heading<?php echo get_the_ID(); ?>" data-parent="#faq_collapse">
                                    <div class="card-body">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
