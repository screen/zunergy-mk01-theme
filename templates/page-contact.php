<?php
/**
* Template Name: Template - Contacto
*
* @package zunergy
* @subpackage zunergy-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="main-contacto-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="main-contacto-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="main-contacto-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h1><?php the_title(); ?></h1>
                        </div>
                        <div class="main-contacto-form-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="row justify-content-between">
                                <div class="main-contacto-form-item col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                                    <div class="custom-contact-form-control col-12">
                                        <label for="nombre"><?php _e('Nombre', 'zunergy')?> <span class="required">*</span>
                                            <input type="text" name="nombre" class="form-control" required />
                                        </label>
                                    </div>
                                    <div class="custom-contact-form-control col-12">
                                        <label for="correo"><?php _e('Correo Electrónico', 'zunergy')?> <span class="required">*</span>
                                            <input type="text" name="correo" class="form-control" required />
                                        </label>
                                    </div>
                                    <div class="custom-contact-form-control col-12">
                                        <label for="telefono"><?php _e('Número de teléfono', 'zunergy')?> <span class="required">*</span>
                                            <input type="text" name="telefono" class="form-control" required title="<?php _e('Completa este campo', 'zunergy'); ?>"/>
                                        </label>
                                    </div>
                                    <div class="custom-contact-form-control col-12">
                                        <label for="comments"><?php _e('Comentarios', 'zunergy')?>
                                            <textarea name="comments" id="" cols="30" rows="4" class="form-control" title="<?php _e('Completa este campo, por favor sea descriptivo', 'zunergy'); ?>"></textarea>
                                        </label>
                                    </div>
                                    <div class="custom-contact-form-control col-12">
                                        <button type="submit" class="btn btn-md btn-contact-submit" title="<?php _e('Haga click aquí para enviar su mensaje', 'zunergy'); ?>"><?php _e('Enviar', 'zunergy'); ?></button>
                                    </div>
                                </div>
                                <div class="main-contacto-form-item col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="main-contact-data-wrapper">
                                        <h3><?php _e('Dirección', 'zunergy'); ?></h3>

                                        <div class="main-contacto-sub-item col-12">
                                            <div class="row align-items-center">
                                                <div class="main-contacto-sub-item-image col-3">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/contact_icon_01.png" alt="Dirección" class="img-fluid" />
                                                </div>
                                                <div class="main-contacto-sub-item-text col-9">
                                                    <?php $content = get_post_meta(get_the_ID(), 'zgy_contacto_info_dir', true); ?>
                                                    <?php echo apply_filters('the_content', $content); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="main-contacto-sub-item col-12">
                                            <div class="row align-items-center">
                                                <div class="main-contacto-sub-item-image col-3">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/contact_icon_02.png" alt="Dirección" class="img-fluid" />
                                                </div>
                                                <div class="main-contacto-sub-item-text col-9">
                                                    <?php $content = get_post_meta(get_the_ID(), 'zgy_contacto_info_telf', true); ?>
                                                    <?php echo apply_filters('the_content', $content); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="main-contacto-sub-item col-12">
                                            <div class="row align-items-center">
                                                <div class="main-contacto-sub-item-image col-3">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/contact_icon_03.png" alt="Dirección" class="img-fluid" />
                                                </div>
                                                <div class="main-contacto-sub-item-text col-9">
                                                    <?php $content = get_post_meta(get_the_ID(), 'zgy_contacto_info_email', true); ?>
                                                    <?php echo apply_filters('the_content', $content); ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="hexagon-container"></div>
                            <div class="hexagon-container second-hexagon"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-contacto-image col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
        </section>
    </div>
</main>
<?php get_footer(); ?>
